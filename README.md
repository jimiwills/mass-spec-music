# mass-spec-music

convert orbitrap mass spectra back to frequencies, but divided to audible range.


Depends on raw2peaks to get peaks out of thermo raw files.

Convert using something like:

    ~/git/Raw2Peaks/bin/Debug/Raw2Peaks.exe --printall --filtertext "FTMS -" --thresh 5000 --minpeakpoints 20 06_SDHB-Cells-2.raw > 6.tsv

or

    ~/git/Raw2Peaks/bin/Debug/Raw2Peaks.exe --printall --filtertext "FTMS -" --thresh 5000 --minpeakpoints 20 07_NT-Cells-2.raw > 7.tsv

Then generate the waveforms, which are dumped to a wav...

    perl peaks2waveforms.pl 6.tsv 7.tsv

The two files goes to left and right channels.

At the time of writing (and first commit; 2019-09-23 12:01 BST) there are various interesting parameters that can be modified within the script.  The plan is to make these configurable on the command line, although there's another completing plan to re-write the whole thing, including functionality from raw2peaks, in csharp)



