#!/usr/bin/perl

use strict;
use warnings;


=pod

a peak needs to have:
start time
end time
rt/mz/int for each time point

then we can interpolate mz and int for each sample

fade-in/out (pad with zeroes)
normalize (and maybe also do that on whole-file level)
convert mz to f (or theta-step)

ts = t1 + s*(t2-t1)/r

=cut

my $timefactor = 10; # 1 minute becomes 6 seconds / one unit is 6 seconds
my $maxpeakduration = 3 * $timefactor; # 2 minutes
my $minrt = 6 * $timefactor; # 6 minutes
my $maxrt = 18 * $timefactor; # 18 minutes

my $minimumpeakintensity = 1e5;


my $log_ic = 0;

$minimumpeakintensity = log($minimumpeakintensity) if $log_ic;


my $freqmult = 1e5/2;

my $tailfactor = 5;

my $minmz = 116;
my $maxmz = 122;

my $maxwaveintensity = 30000;

my $fn = pop @ARGV;
open(my $fh, '<', $fn) or die $!;

my $lastrt = 0;

############# read in file 1

my @peaks = ();
my %tic = ();
while(! eof($fh)){
	my $peak = readpeak($fh, $timefactor, $log_ic);
	last unless $peak;
	next unless ($peak->[1] >= $minmz && $peak->[1] <= $maxmz);
	next unless (abs($peak->[1]-117.019) < 0.01 || abs($peak->[1]-117.019+1.003355*4) < 0.01);
	my $peakstart = $peak->[2]->[0];
	my $peakend = $peak->[$#$peak]->[0];
	next unless ($peakstart >= $minrt && $peakend <= $maxrt);
	my $peakduration = $peakend - $peakstart;
	next if $peakduration > $maxpeakduration;
	my $lastpeakrt = $peak->[$#$peak]->[0];
	$lastrt = $lastpeakrt if $lastpeakrt > $lastrt;
	my $maxic = 0;
	foreach my $i(2..$#$peak){
		my $rt = $peak->[$i]->[0];
		my $ic = $peak->[$i]->[2];
		$maxic = $ic if $ic > $maxic;
		$tic{$rt} = 0 unless defined $tic{$rt};
		$tic{$rt} += $ic;
	}
	next unless $maxic > $minimumpeakintensity;
	push @peaks, $peak;
}
close($fh);

####### read in file 2

my $fn2 = pop @ARGV;
open(my $fh2, '<', $fn2) or die $!;
my @peaks2 = ();
while(! eof($fh2)){
	my $peak = readpeak($fh2, $timefactor, $log_ic);
	last unless $peak;
	next unless ($peak->[1] >= $minmz && $peak->[1] <= $maxmz);
	my $peakstart = $peak->[2]->[0];
	my $peakend = $peak->[$#$peak]->[0];
	next unless ($peakstart >= $minrt && $peakend <= $maxrt);
	my $peakduration = $peakend - $peakstart;
	next if $peakduration > $maxpeakduration;
	my $lastpeakrt = $peak->[$#$peak]->[0];
	$lastrt = $lastpeakrt if $lastpeakrt > $lastrt;
	my $maxic = 0;
	foreach my $i(2..$#$peak){
		my $rt = $peak->[$i]->[0];
		my $ic = $peak->[$i]->[2];
		$maxic = $ic if $ic > $maxic;
		$tic{$rt} = 0 unless defined $tic{$rt};
		$tic{$rt} += $ic;
	}
	next unless $maxic > $minimumpeakintensity;
	push @peaks2, $peak;
}
close($fh2);


my $maxtic = 0;
foreach (values %tic){
	$maxtic = $_ if $_ > $maxtic;
}

my $scale_factor =  $maxwaveintensity / $maxtic;
my $samplerate = 44100;
my $channels = 2;
my $totalsamples = $samplerate * $lastrt;
my $bitspersample = 16;


open(my $fo, '+>', "$fn.$fn2.$minmz-$maxmz.wav") or die $!;
binmode($fo);
WavPCM::write_header($fo, 'bits'=>$bitspersample, 'channels'=>$channels);


$|++;


my $j = 0;


my $c = 0;
my $npeaks = @peaks;
foreach my $peak(@peaks){
	my $rtstart = $peak->[2]->[0];
	my $samplestart = int(($rtstart-$minrt) * $samplerate);
	my $filestart = $samplestart * $channels + 44;
	my $wave = peak2waveform($peak, $samplerate, $freqmult, $tailfactor);
	scale_wave_intensity($wave, $scale_factor);
	my @wavesamples = @$wave;
	shift @wavesamples;
	my $L = @wavesamples;

	my $buf;
	seek($fo, $filestart, 0);
	read($fo, $buf, $L*$channels*$bitspersample/8);
	my @filesamples = unpack('s*', $buf);

	# these might be different in the future
	my @channels = (\@wavesamples, \@wavesamples);
	
	my $nfilesamples = $L * $channels;

	while(@filesamples < $nfilesamples){
		push @filesamples, 0;
	}

	foreach my $i(0..$#wavesamples){
		$filesamples[$i*$channels+$j] +=
			$channels[$j]->[$i];
		if(abs($filesamples[$i*$channels+$j])
		 > $maxwaveintensity){
			warn "$filesamples[$i*$channels+$j] > $maxwaveintensity (maxwaveintensity)";
		}
	}
	seek($fo, $filestart, 0);
	print $fo pack("s*", @filesamples);
	$c++;
	print STDERR "\r$c/$npeaks";
}
print STDERR "\nDone\n";


######################


$j = 1;


$c = 0;
$npeaks = @peaks2;
foreach my $peak(@peaks2){
	my $rtstart = $peak->[2]->[0];
	my $samplestart = int(($rtstart-$minrt) * $samplerate);
	my $filestart = $samplestart * $channels + 44;
	my $wave = peak2waveform($peak, $samplerate, $freqmult, $tailfactor);
	scale_wave_intensity($wave, $scale_factor);
	my @wavesamples = @$wave;
	shift @wavesamples;
	my $L = @wavesamples;

	my $buf;
	seek($fo, $filestart, 0);
	read($fo, $buf, $L*$channels*$bitspersample/8);
	my @filesamples = unpack('s*', $buf);

	# these might be different in the future
	my @channels = (\@wavesamples, \@wavesamples);
	
	my $nfilesamples = $L * $channels;

	while(@filesamples < $nfilesamples){
		push @filesamples, 0;
	}

	foreach my $i(0..$#wavesamples){
		$filesamples[$i*$channels+$j] +=
			$channels[$j]->[$i];
		if(abs($filesamples[$i*$channels+$j])
		 > $maxwaveintensity){
			warn "$filesamples[$i*$channels+$j] > $maxwaveintensity (maxwaveintensity)";
		}
	}
	seek($fo, $filestart, 0);
	print $fo pack("s*", @filesamples);
	$c++;
	print STDERR "\r$c/$npeaks";
}
print STDERR "\nDone\n";



##################




#WavPCM::finalise($fo);
close($fo);

sub limit_wave {
	my ($wave, $limit, $increase) = @_;
	$increase = 0 unless defined $increase;
	# increase is boolean, should we increase
	# so max is at limit?
	my $max = 0;
	foreach my $i(1..$#$wave){ # 0 is metadata
		my $absval = abs($wave->[$i]);
		$max = $absval if $absval > $max;
	}
	return $wave unless $max > $limit || $increase;
	my $f = $limit / $max;
	return scale_wave_intensity($wave, $f);
	foreach my $i(1..$#$wave){ # 0 is metadata
		$wave->[$i] *= $f;
	}
	return $wave;
}


sub scale_wave_intensity {
	my ($wave, $f) = @_;
	foreach my $i(1..$#$wave){ # 0 is metadata
		$wave->[$i] = int($wave->[$i] * $f);
	}
	return $wave;
}

sub readpeak {
	my ($fh, $timefactor, $log_ic) = @_;
	$timefactor = 1 unless defined $timefactor;
	$log_ic = 0 unless defined $log_ic;
	my @samples = ();
	while(<$fh>){
		if(/^([\d.]+)\t([\d.]+)\t([\d.]+)/){
			## WARNING:
			#   rt in file is minutes
			#   convert to seconds!
			push @samples, [$1*$timefactor,$2, $log_ic ? log($3) : $3];
		}
		return [$1,$2,@samples] if /PEAK_\d+\t([\d.]+)\t([\d.]+)/;
	}
}


sub peak2waveform {
	my ($peak, $rate, $freqmult, $tailfactor) = @_;
	$rate = 44100 unless defined $rate;
	$freqmult = 1e5 unless defined $freqmult;
	$freqmult *= 3.1415926535897932384626433 * 2;
	# extract meta
	my $peak_rt = shift @$peak;
	my $peak_mz = shift @$peak;
	# start and finish conditions:
	my $rt_start = $peak->[0]->[0];
	my $mz_start = $peak->[0]->[1];
	my $rt_end = $peak->[$#$peak]->[0];
	my $mz_end = $peak->[$#$peak]->[1];
	# calculate average peak sample duration
	my $rt_length = $rt_end - $rt_start;
	my $peak_samples = @$peak;
	my $mean_peak_sample_dur = $rt_length/($peak_samples-1);
	# insert fakes
	$rt_start -= $mean_peak_sample_dur;
	$rt_end += $mean_peak_sample_dur * $tailfactor;
	unshift(@$peak, [$rt_start, $mz_start, 0]);
	push(@$peak, [$rt_end, $mz_end, 0]);
	$rt_length += 2;
	# interpolate
	my $theta = 0;
	my @wave = ();
	foreach my $j(1..$#$peak){
		# details about each peak...
		my $rt1 = $peak->[$j-1]->[0];
		my $mz1 = $peak->[$j-1]->[1];
		my $ic1 = $peak->[$j-1]->[2];
		my $rt2 = $peak->[$j]->[0];
		my $mz2 = $peak->[$j]->[1];
		my $ic2 = $peak->[$j]->[2];
		# details about this interval
		my $interval_dur = $rt2 - $rt1;
		my $interval_samples = int($interval_dur * $rate);


		foreach my $i(0..$interval_samples){
			my $fraction2 = $i/$interval_samples;
			my $fraction1 = 1 - $fraction2;
			my $mz = $mz1 * $fraction1 + $mz2 * $fraction2;
			my $ic = $ic1 * $fraction1 + $ic2 * $fraction2;
			my $delta_theta = $freqmult/($mz * $rate);
			$theta += $delta_theta;
			#print "$j $i/$interval_samples $fraction1 $fraction2 $ic1 $ic2 $ic\n";
			push @wave, $ic * sin($theta);
			#print "$ic\n";
		}
	}
	die "wave start and end are not zero..."
		unless $wave[0] == 0 && $wave[$#wave] == 0;
	return [[$peak_rt, $peak_mz], @wave];
}



package WavPCM;

sub write_header {
	my ($fh,%opts) = @_;
	my %defaults = (
		rate => 44100,
		channels => 2,
		bits => 16, # bits per sample
	);
	%opts = (%defaults, %opts);
	binmode($fh);
	print $fh pack("A4LA4A4LS2L2S2A4L",
		"RIFF", # A4
		0, # L initially, don't foget to finalise!
		"WAVE", # A4
		"fmt ", # A4
		16,   # L  length of fmt section
		1,    # S  type PCM
		$opts{'channels'}, # S
		$opts{'rate'}, # L
		$opts{'rate'} * $opts{'channels'}
				 * $opts{'bits'} / 8, # L
		$opts{'channels'} * $opts{'bits'} / 8, # S
		$opts{'bits'}, # S
		"data", # A4
		0, 		# L
	);
}

sub finalise {
	my ($fh) = @_;
	# check we're at the end...
	seek($fh, 0, 2);
	# grab posn
	my $L = tell($fh);
	# fill in the numbers...
	seek($fh, 5, 0);
	print $fh pack("L", $L-8);
	seek($fh, 41, 0);
	print $fh pack("L", $L-44);
}


